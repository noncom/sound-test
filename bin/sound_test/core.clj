(ns sound-test.core
  (:require [quil.core :as q]
            [overtone.music.pitch :as pitch])
  (:import sound_test.SoundWork))

(def w 800)
(def h 600)

(def sw (atom nil))
(def g (atom nil))

(def d-max (atom 1))

(def scan-line (atom 0))

(def signal-bands (atom []))          ;; contains a [[] ..] of indices of the bands where signal is present on the current frame
(def current-bands-group (atom []))   ;; contains indices of the band group currently being accumulated (temporary acc)
(def signal-freqs (atom []))          ;; contains a [[] ..] of freqs of the signal bands
(def current-freqs-group (atom []))   ;; same as for bands, but contains the freq
(def signal-dominants (atom []))      ;; contains [float ...] of dominant freqs in band groups

(def last-measured-band-idx (atom -1)) ; temporary counter, the idx of the last measured band in the current frame.

(def threshold 0.2)

(def image-height-mult 3)

(defn setup []
  ;(q/frame-rate 1)
  (q/background 200 20)
  ; - sound
  (reset! sw (SoundWork. (* 1 1024) 22050))
  (.start @sw)
  ; - graphics
  (reset! g (q/create-image w h :argb))
  )

(defn average [nums]
  (float (/ (float (apply + nums)) (float (count nums)))))


(def note-x (/ w 2))
(def note-y (- h 45))

(def pos-k 100)
(def pos-k2 (* pos-k 2))
(def pos-k3 (* pos-k 3))

(defn draw-note [note amplitude]
 ; (println "note = " note)
  (let [note (name note)
        note (.replace note "#" "")
        note (.replace note "b" "")
        note (.replace note "-" "")
        ;_ (println "res note = " note)
        [n s] (name note)
        n (str n)
        s (str s)
        
        
        ;[n m? s] (name note)
        ;n (str n)
        ;s (str s)
        ;m? (str m?)
        ;s (if (= m? "#") s m?)
        ;s (if (= m? "-") "-1" m?)
        
        ;_ (println "S = " s " / " (= m? "-"))
        size (Integer/parseInt s)
        size (* 5 size)
        size2 (* size 2)
        ;alpha (- 255 size)
        
        alpha (q/map-range (* 10 amplitude) 0 @d-max 0 255)
        ;alpha (+ alpha k)
        ]
    ;(println "k = " alpha)
    (case n
      "C" (do
            (q/stroke 255 100 50 alpha)
            ;(q/rect (- note-x size) (- note-y size) size2 size2)
            (q/ellipse (- note-x pos-k3) note-y size2 size2)
            )
      "D" (do
            (q/stroke 50 200 70 alpha)
            (q/ellipse (- note-x pos-k2) note-y size2 size2))
      "E" (do
            (q/stroke 80 100 221 alpha)
            ;(q/triangle (- note-x size) (+ (/ size 2) note-y) note-x (- note-y size) (+ note-x size) (+ (/ size 2) note-y))
            (q/ellipse (- note-x pos-k) note-y size2 size2)
            )
      "F" (do
            (q/stroke 250 100 221 alpha)
            (q/ellipse note-x note-y size2 size2)
            ) 
      #_(doseq [i (range 0 2 0.1)]
         (let [x (+ note-x (* size (q/sin (* i q/PI))))
               y (+ note-y (* size (q/cos (* i q/PI))))]
           (q/line note-x note-y x y)))
      "G" (do
            (q/stroke 150 230 102 alpha)
            (q/ellipse (+ note-x pos-k) note-y size2 size2))
      "A" (do
            (q/stroke 230 230 102 alpha)
            (q/ellipse (+ note-x pos-k2) note-y size2 size2))
      "B" (do
            (q/stroke 150 230 102 alpha)
            (q/ellipse (+ note-x pos-k3) note-y size2 size2))
      nil)))

(defn draw []
  ;(q/background 0 20 40 1)
  
  (q/fill 0 50)
  (q/rect 0 0 w h)
  
  (q/color-mode :hsb)
  ;  (println "g = " @g)
  (let [data (into [] (.getData @sw))
        data-size (count data)
        maximum (apply max data)
        soundworks @sw
        x @scan-line
        pixels (q/pixels @g)
        data-width (/ (- (count data) 2) 3)
        screen-width (dec w)
        max-width (min data-width screen-width)
        max-height (min data-width (dec h))
        res-img-height (* h image-height-mult)
        res-max-height (* max-height image-height-mult)]
    ; -- reset accs
    (when (> maximum @d-max) (reset! d-max maximum))
    (reset! signal-bands [])
    (reset! signal-freqs [])
    (reset! signal-dominants [])
    ; -- horizontal
    (doseq [y (range (- max-height 1))]
      (let [
            i (- max-height y)
            d (nth data i)
            dd (- 255 (q/map-range (* 3 d) 0 @d-max 0 255))
           ;; dd (q/map-range (* 3 d) 0 @d-max 0 255)
            brightness (if (< dd 30) 30 (- 255 dd)) 
            c (q/color dd 255 brightness)
            ids [(+ (* y w) x)
                 ;(+ (* y w) (+ 1 (* 3 x)))
                 ;(+ (* y w) (+ 2 (* 3 x)))
                 ]]
        ; -- perform data acc for analysis
        (if (> d threshold)
          (do
            (swap! current-bands-group conj i)
            (swap! current-freqs-group conj (.getBandFrequency soundworks i)))
          (do
            (swap! signal-bands conj @current-bands-group)
            (reset! current-bands-group [])
            (swap! signal-freqs conj @current-freqs-group)
            (reset! current-freqs-group [])))
        ; -- draw band
        (doseq [idx ids]
          (aset pixels idx c))
        ))
    ; -- vertical
    #_(doseq [x (range max-width)]
        (let [y @scan-line
              d (nth data x)
              dd (- 255 (q/map-range d 0 @d-max 0 255))
              c (q/color dd 255 (- 300 dd))
              ids [(+ (* y w) x)
                   ;(+ (* y w) (+ 1 (* 3 x)))
                   ;(+ (* y w) (+ 2 (* 3 x)))
                   ]]
          (doseq [idx ids]
            (aset pixels idx c))
          ))
    (q/update-pixels @g)
    ; -- main image
    (q/image @g 0 0 w res-img-height)
    ; -- reset color mode
    (q/color-mode :rgb)

    ; -- freq analysis
    ; ---- eliminate zero-length inclusions
    (swap! signal-freqs #(filter (comp not empty?) %))
    (swap! signal-bands #(filter (comp not empty?) %))
    ;(println signal-freqs)
    ; ---- main analysis and display
    
    (doseq [ii (-> @signal-bands count range)]
      (let [avg-freq (average (nth @signal-freqs ii))
            avg-band (average (nth @signal-bands ii))
            amplitude (nth data avg-band)
            y (+ res-max-height (- (* image-height-mult avg-band)))
            note (-> avg-freq pitch/hz->midi pitch/find-note-name)
            ]
            (q/stroke 255)
            (q/fill 255) 
            (q/color 255 255 255) 
            (q/rect x y 3 3)
            (q/text (str (int avg-freq) " Hz / " note) (+ x 20) y)
            (q/no-fill) 
            (draw-note note amplitude)))
    
    
    ; --
    (swap! scan-line inc)
    (when (> @scan-line (- w 2)) (reset! scan-line 0))
    ; --
    
    (q/fill 0 0 0)
    ;(q/text (str "MAX = " @d-max) 100 300)
    ;(q/text (str "MAX L = " maximum) 100 320)
    )
  
  #_(let [diam (q/random 100)
          x    (q/random (q/width))
          y    (q/random (q/height))
          data (into [] (.getData @sw))
          data-size (count data)
          maximum (apply max data)]
      ;  (println "data size = " (count data) " max = " maximum " min = " (apply min data))
      (when (> maximum @d-max) (reset! d-max maximum)) 
      (q/ellipse x y diam diam)
      (q/stroke 200 50 60)
      #_(doseq [i (range (- (count data) 2))]
          (let [a (nth data i)
                b (nth data (inc i))]
            (q/line i (* a 100) (inc i) (* 100 b))))
      
     
      )
  (swap! d-max / 2)
  (when (> 1 @d-max) (reset! d-max 1))
  )

(defn destroy []
  (.destroy @sw)
  (reset! sw nil)
  (reset! d-max 1)
  (println "DESTROY"))

(defn run []
  (q/defsketch example
    :title "Oh so many grey circles"
    :settings #(q/smooth 2)
    :setup #'setup
    :draw #'draw
    :on-close #'destroy
    :size [w h]))