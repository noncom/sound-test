package sound_test;

import ddf.minim.AudioInput;
import ddf.minim.DefaultFileSystemHandler;
import ddf.minim.Minim;
import ddf.minim.analysis.FFT;

public class SoundWork {
	
	Minim minim;
	AudioInput in;
	FFT fft;
	
	float[] data;
	
	int bufferSize;
	int sampleRate;
	
	public SoundWork(int bufferSize, int sampleRate) {
		minim = new Minim(new DefaultFileSystemHandler());
		this.bufferSize = bufferSize;
		this.sampleRate = sampleRate;
	}
	
	public void start() {
		in = minim.getLineIn(Minim.STEREO, bufferSize, sampleRate);
		fft = new FFT(in.bufferSize(), in.sampleRate());
		fft.window(FFT.HAMMING);
		data = new float[fft.specSize()];
	}
	
	public float[] getData() {
		fft.forward(in.mix);
		for (int i = 0; i < fft.specSize(); i++) {
			data[i] = fft.getBand(i);
		}
		return data;
	}
	
	public float getBandFrequency(int band) {
		//float avg = fft.getAverageBandWidth(band);
		float avg = fft.indexToFreq(band);
		return avg;
	}
	
	public void destroy() {
		in.close();
		in = null;
		fft = null;
		minim.stop();
		minim.dispose();
		minim = null;
	}
}
